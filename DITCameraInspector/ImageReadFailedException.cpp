#include "ImageReadFailedException.h"

using namespace dit_camera_inspector;


ImageReadFailedException::ImageReadFailedException()
{
	SetErrorMessage();
}
ImageReadFailedException::ImageReadFailedException(string error_message) : BaseCustomException(error_message)
{

}
void ImageReadFailedException::SetErrorMessage()
{
	m_error_message = "image read failed";
}