#include "AlgorithmNotRegisteredException.h"

using namespace dit_camera_inspector;


AlgorithmNotRegisteredException::AlgorithmNotRegisteredException()
{
	SetErrorMessage();
}
AlgorithmNotRegisteredException::AlgorithmNotRegisteredException(string error_message) : BaseCustomException(error_message)
{

}
void AlgorithmNotRegisteredException::SetErrorMessage()
{
	m_error_message = "algorithm is not registered";
}