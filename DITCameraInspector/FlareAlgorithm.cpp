#include "FlareAlgorithm.h"


unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, FlareAlgorithm>> FlareAlgorithm::m_is_registered = AlgorithmRegistrar<IImageInspectionAlgorithm, FlareAlgorithm>::CreateRegistrar(FlareAlgorithm::GetTypeName());

FlareAlgorithm::FlareAlgorithm(Spec* p_spec)
{
	m_patch_length = stoi(p_spec->m_content_map["Flare"]["PatchLength"]);
    m_lapla_thresh = stod(p_spec->m_content_map["Flare"]["LaplacianScore"]);
	m_number_of_row_segs = 0;
	m_number_of_col_segs = 0;
	m_number_of_patches = 0;
	m_final_lapla_score = 0.0;
	m_type_name = "Flare";
}
void FlareAlgorithm::Run(Mat img)
{
    Logger(INFO) << "Flare Inspecting...";

	// Init mat data
	m_src_image = img.clone();
	m_out_image = img.clone();

	// To gray scale and filter by Laplacian
    Mat gray_src;
    cvtColor(m_src_image, gray_src, CV_RGB2GRAY);
    Laplacian(gray_src, m_lapla_image, CV_8UC1);

	SplitByPatchLength();

	ComputePatchCoordinate();

	InspectAndDrawResultImage();

	ComputeFinalScore();

	Logger(DEBUG) << "Final score: " << m_final_lapla_score;
    Logger(DEBUG) << "Final result: " << m_is_passed;
}

void FlareAlgorithm::SplitByPatchLength()
{
	// If image lengths are not divisible, the patches located at the right edge and bottom edge 
	// will have larger size for including remained area 

	m_number_of_row_segs = m_src_image.rows / m_patch_length;
	m_number_of_col_segs = m_src_image.cols / m_patch_length;
	m_number_of_patches = m_number_of_row_segs * m_number_of_col_segs;

	int remainder_of_rows = m_src_image.rows % m_patch_length;
	int remainder_of_cols = m_src_image.cols % m_patch_length;

	// Initialize patches with zero mat 
	for (int i = 0; i < m_number_of_patches; i++)
	{
		int patch_length_rows = m_patch_length;
		int patch_length_cols = m_patch_length;

		// If the patch is located at the right edge
		if ((i + 1) % m_number_of_col_segs == 0)
		{
			patch_length_cols += remainder_of_cols;
		}
		// If the patch is located at the bottom edge 
		if (i >= m_number_of_patches - m_number_of_col_segs)
		{
			patch_length_rows += remainder_of_rows;
		}
		Mat patch = Mat::zeros(patch_length_rows, patch_length_cols, CV_8UC1);
		m_vec_patches.push_back(patch);
	}

	// Assign value for each patch
	for (int i = 0; i < m_lapla_image.rows; i++)
	{
		for (int j = 0; j < m_lapla_image.cols; j++)
		{

			int row_seg_index = i / m_patch_length;
			int col_seg_index = j / m_patch_length;

			// If the patch is located at the right edge
			if (i >= m_number_of_row_segs * m_patch_length)
			{
				row_seg_index--;
			}
			// If the patch is located at the bottom edge 
			if (j >= m_number_of_col_segs * m_patch_length)
			{
				col_seg_index--;
			}
			int patch_index = row_seg_index * m_number_of_col_segs + col_seg_index;
			int patch_row = i - row_seg_index * m_patch_length;
			int patch_col = j - col_seg_index * m_patch_length;

			m_vec_patches[patch_index].at<uchar>(patch_row, patch_col) = m_lapla_image.at<uchar>(i, j);
		}
	}
}

void FlareAlgorithm::ComputePatchCoordinate()
{
	for (int i = 0; i < m_number_of_patches; i++)
	{
		int row_seg_index = i / m_number_of_col_segs;
		int col_seg_index = i % m_number_of_col_segs;
		int patch_topleft_x = col_seg_index * m_patch_length + 0;
		int patch_topleft_y = row_seg_index * m_patch_length + 0;
		int patch_bottomright_x = col_seg_index * m_patch_length + m_vec_patches[i].cols;
		int patch_bottomright_y = row_seg_index * m_patch_length + m_vec_patches[i].rows;
		m_vec_patch_coordinates.push_back(make_pair(Point(patch_topleft_x, patch_topleft_y), Point(patch_bottomright_x - 1, patch_bottomright_y - 1)));
	}
}

void FlareAlgorithm::InspectAndDrawResultImage()
{
	for (int i = 0; i < m_number_of_patches; i++)
	{
		// Use mean value as Laplacian score for each patch for inspection
		double score = mean(m_vec_patches[i])[0];
		if (score < m_lapla_thresh)
		{
			m_is_passed = false;

			// Draw the result image
			Point pt1 = m_vec_patch_coordinates[i].first;
			Point pt2 = m_vec_patch_coordinates[i].second;
			Mat roi = m_out_image(Rect(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y));
			Mat color(roi.size(), CV_8UC3, Scalar(0, 0, 255));
			double alpha = 0.3;
			addWeighted(color, alpha, roi, 1.0 - alpha, 0.0, roi);
			
			m_vec_fail_lapla_patch_scores.push_back(score);
		}
		m_vec_lapla_patch_scores.push_back(score);
	}
}

void FlareAlgorithm::ComputeFinalScore()
{
	if (m_is_passed)
		m_final_lapla_score = accumulate(m_vec_lapla_patch_scores.begin(), m_vec_lapla_patch_scores.end(), 0.0) / m_vec_lapla_patch_scores.size();
	else
		m_final_lapla_score = accumulate(m_vec_fail_lapla_patch_scores.begin(), m_vec_fail_lapla_patch_scores.end(), 0.0) / m_vec_fail_lapla_patch_scores.size();
}

void FlareAlgorithm::RecordAndPrintResult()
{
	// Save result message
	m_result_vector.push_back("Flare,0,0,0," + ToPassOrFailString(m_is_passed));
	m_result_vector.push_back("Flare_LaplacianScore,"
		+ to_string(m_final_lapla_score) + ",INFINITY,"
		+ to_string(m_lapla_thresh) + ","
		+ ToPassOrFailString(m_is_passed));

	// Print result message
	for (auto it = m_result_vector.begin(); it != m_result_vector.end(); it++)
	{
		Logger(INFO) << *it;
	}
}

string FlareAlgorithm::GetTypeName()
{
	return "Flare";
}