#include <iostream>
#include <fstream>
#include <map>
#include <cstdbool>
#include <memory>

#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "Logger.h"
#include "Parser.h"
#include "Spec.h"
#include "DataLoader.h"
#include "IImageInspectionAlgorithm.h"
#include "ShadingAlgorithm.h"
#include "BlemishAlgorithm.h"
#include "FlareAlgorithm.h"
#include "IBaseAlgorithmRegistrar.h"
#include "AlgorithmRegistry.h"
#include "AlgorithmRegistrar.h"
#include "Inspector.h"

using namespace std;
using namespace cv;
using namespace dit_camera_inspector;


// Example commands
// DITCameraInspector.exe -LS[Shading] D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/Dataset/DITCameraTestApp/CameraSpec.spe D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/test_pic/shading.jpg
// DITCameraInspector.exe -BL[Blemish] D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/Dataset/DITCameraTestApp/CameraSpec.spe D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/test_pic/blemish.jpg
// DITCameraInspector.exe -FL[Flare] D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/Dataset/DITCameraTestApp/CameraSpec.spe D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/test_pic/flare.jpg

void RunInspector(int argc, char* argv[])
{
	// Parse arguments
	auto p_args = make_shared<Parser>(argc, argv);
	if (p_args->ParseArgs())
	{
		// Inspect the target image
		auto p_inspector = make_shared<Inspector>(p_args);
		p_inspector->Run();
	}
}

int main(int argc, char* argv[])
{
	Logger::SetVisibleLevel(DEBUG);
	try
	{
		RunInspector(argc, argv);
	}
	catch(const bad_alloc& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const ImageReadFailedException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const ios_base::failure& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const DividedByZeroException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const AlgorithmNotRegisteredException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const InvalidArgumentException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	//catch (const exception& e)
	//{
	//	Logger(ERROR) << "Exception occured: " << e.what();
	//}
	// Only in release version
	//catch (...)
	//{
	//	Logger(ERROR) << "Unknown exception occured.";
	//}

	system("pause");
	return 0;
}