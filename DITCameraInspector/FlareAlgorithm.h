#ifndef FLARE_ALGORITHM
#define FLARE_ALGORITHM


#include <string>
#include <vector>
#include <numeric>
#include <memory>

#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "IImageInspectionAlgorithm.h"
#include "Spec.h"
#include "Logger.h"
#include "DataLoader.h"
#include "AlgorithmRegistrar.h"

using namespace std;
using namespace cv;
using namespace dit_camera_inspector;

namespace dit_camera_inspector
{
    class FlareAlgorithm : public IImageInspectionAlgorithm
    {
        public:
            FlareAlgorithm(Spec*);
            void Run(Mat);
			void RecordAndPrintResult();

        private:    
			void SplitByPatchLength();
			void ComputePatchCoordinate();
			void InspectAndDrawResultImage();
			void ComputeFinalScore();
			static string GetTypeName();

			static unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, FlareAlgorithm>> m_is_registered;
			int m_patch_length;
            double m_lapla_thresh;
			int m_number_of_patches;
			int m_number_of_row_segs;
			int m_number_of_col_segs;
			Mat m_lapla_image;
			vector<Mat> m_vec_patches;
			vector<pair<Point, Point>> m_vec_patch_coordinates;
			vector<double> m_vec_lapla_patch_scores;
			vector<double> m_vec_fail_lapla_patch_scores;
			double m_final_lapla_score;
    };  
}
#endif // FLARE_ALGORITHM