#pragma once

#include <exception>
#include <string>

using namespace std;


namespace dit_camera_inspector
{
	class BaseCustomException : public exception
	{
	public:
		virtual ~BaseCustomException(){}
		const char* what() const;

	protected:
		BaseCustomException();
		BaseCustomException(string);
		virtual void SetErrorMessage() = 0;
		
		string m_error_message;
	};
}

