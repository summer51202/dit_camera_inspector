#include "ShadingAlgorithm.h"

unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, ShadingAlgorithm>> ShadingAlgorithm::m_is_registered = AlgorithmRegistrar<IImageInspectionAlgorithm, ShadingAlgorithm>::CreateRegistrar(ShadingAlgorithm::GetTypeName());

ShadingAlgorithm::ShadingAlgorithm(Spec* p_spec){
    m_pass_level_up = stod(p_spec->m_content_map["Shading"]["PassLevel_Up"]);
    m_pass_level = stod(p_spec->m_content_map["Shading"]["PassLevel"]);
    m_diff = stod(p_spec->m_content_map["Shading"]["Diff"]);
    m_center_up = stod(p_spec->m_content_map["Shading"]["Center_Up"]);
    m_center_low = stod(p_spec->m_content_map["Shading"]["Center_Low"]);
	m_yc = 0.0;
	m_type_name = "Shading";
	m_is_passed = false;
}
void ShadingAlgorithm::Run(Mat img)
{
    Logger(INFO) << "Shading Inspecting...";

	// Init mat data
	m_src_image = img.clone();
	m_out_image = img.clone();

    GetCenterRegion();

    GetCornerRegions();

    ComputeAllAverageYValue();

	m_is_passed = CheckCenterValue() && CheckPassLevelValue() && CheckDiffValue();
    
    Logger(DEBUG) << "Y values: " << m_yc << " " << m_vec_ys[0] << " " << m_vec_ys[1] << " "  << m_vec_ys[2] << " " << m_vec_ys[3];
    Logger(DEBUG) << "Fail conditions: " << CheckCenterValue() << " " << CheckPassLevelValue() << " " << CheckDiffValue();
    Logger(DEBUG) << "Final result: " << m_is_passed;
}
void ShadingAlgorithm::GetCenterRegion()
{
    // Find the maximum of the sum of RGB value in the image
    int y_value = 0;
    int max_y_value = 0;
    Point max_y_index;
    int center_row_begin = m_src_image.rows/4, center_row_end = m_src_image.rows*3/4;
    int center_col_begin = m_src_image.cols/4, center_col_end = m_src_image.cols*3/4;
    for(int i = center_row_begin; i < center_row_end; i++)
    {
        for(int j = center_col_begin; j < center_col_end; j++)
        {
            y_value = m_src_image.at<Vec3b>(i, j)[0] + m_src_image.at<Vec3b>(i, j)[1] + m_src_image.at<Vec3b>(i, j)[2];
            if(y_value > max_y_value)
            {
                max_y_value = y_value;
                max_y_index.x = i;
                max_y_index.y = j;
            }
        }
    }

    // Sample the center region of the image which the center is the maximum of RGB sum 
    m_center_region = Mat::zeros(m_src_image.rows/10, m_src_image.cols/10, CV_8UC3);
    int max_y_row_begin = max_y_index.x-m_src_image.rows/20, max_y_row_end = max_y_index.x+m_src_image.rows/20;
    int max_y_col_begin = max_y_index.y-m_src_image.cols/20, max_y_col_end = max_y_index.y+m_src_image.cols/20;

	for(int i = max_y_row_begin; i < max_y_row_end; i++)
    {
        for(int j = max_y_col_begin; j < max_y_col_end; j++)
        {
            m_center_region.at<Vec3b>(i-max_y_row_begin, j-max_y_col_begin)[0] = m_src_image.at<Vec3b>(i, j)[0];
            m_center_region.at<Vec3b>(i-max_y_row_begin, j-max_y_col_begin)[1] = m_src_image.at<Vec3b>(i, j)[1];
            m_center_region.at<Vec3b>(i-max_y_row_begin, j-max_y_col_begin)[2] = m_src_image.at<Vec3b>(i, j)[2];
        }
    }

	// Record the center region coordinate
	// Note that the point.x is mat.cols and the point.y is mat.rows
	m_vec_y_coordinates.push_back(make_pair(Point(max_y_col_begin, max_y_row_begin), Point(max_y_col_end - 1, max_y_row_end - 1)));
}

void ShadingAlgorithm::GetCornerRegions()
{
    // Initialize corner region list
    for(int i = 0; i < m_kCornerNumber; i++)
    {
        Mat corner_region = Mat::zeros(m_src_image.rows/10, m_src_image.cols/10, CV_8UC3);
        m_vec_corner_regions.push_back(corner_region);
    }

    // Sample corner regions
    int one_tenth_rows = m_src_image.rows/10;
    int one_tenth_cols = m_src_image.cols/10;

    // Check the correct 9/10 values of rows and cols that cannot be divided by 10
    int nine_tenth_rows = one_tenth_rows*9 + m_src_image.rows%10;
    int nine_tenth_cols = one_tenth_cols*9 + m_src_image.cols%10;

	// Record the corner region coordinates
	m_vec_y_coordinates.push_back(make_pair(Point(0, 0), Point(one_tenth_cols - 1, one_tenth_rows - 1)));
	m_vec_y_coordinates.push_back(make_pair(Point(nine_tenth_cols, 0), Point(m_src_image.cols - 1, one_tenth_rows - 1)));
	m_vec_y_coordinates.push_back(make_pair(Point(0, nine_tenth_rows), Point(one_tenth_cols - 1, m_src_image.rows - 1)));
	m_vec_y_coordinates.push_back(make_pair(Point(nine_tenth_cols, nine_tenth_rows), Point(m_src_image.cols - 1, m_src_image.rows - 1)));

    for(int i = 0; i < m_src_image.rows; i++)
    {
        for(int j = 0; j < m_src_image.cols; j++)
        {
			// Top left corner
            if(i < one_tenth_rows && j < one_tenth_cols)
            {
                m_vec_corner_regions[0].at<Vec3b>(i, j)[0] = m_src_image.at<Vec3b>(i, j)[0];
                m_vec_corner_regions[0].at<Vec3b>(i, j)[1] = m_src_image.at<Vec3b>(i, j)[1];
                m_vec_corner_regions[0].at<Vec3b>(i, j)[2] = m_src_image.at<Vec3b>(i, j)[2];

            }
			// Top right corner
            else if(i < one_tenth_rows && j >= nine_tenth_cols)
            {
                m_vec_corner_regions[1].at<Vec3b>(i, j-nine_tenth_cols)[0] = m_src_image.at<Vec3b>(i, j)[0];
                m_vec_corner_regions[1].at<Vec3b>(i, j-nine_tenth_cols)[1] = m_src_image.at<Vec3b>(i, j)[1];
                m_vec_corner_regions[1].at<Vec3b>(i, j-nine_tenth_cols)[2] = m_src_image.at<Vec3b>(i, j)[2];
            }
			// Bottom left corner
            else if(i >= nine_tenth_rows && j < one_tenth_cols)
            {
                m_vec_corner_regions[2].at<Vec3b>(i-nine_tenth_rows, j)[0] = m_src_image.at<Vec3b>(i, j)[0];
                m_vec_corner_regions[2].at<Vec3b>(i-nine_tenth_rows, j)[1] = m_src_image.at<Vec3b>(i, j)[1];
                m_vec_corner_regions[2].at<Vec3b>(i-nine_tenth_rows, j)[2] = m_src_image.at<Vec3b>(i, j)[2];
            }
			// Bottom right corner
            else if(i >= nine_tenth_rows && j >= nine_tenth_cols)
            {
                m_vec_corner_regions[3].at<Vec3b>(i-nine_tenth_rows, j-nine_tenth_cols)[0] = m_src_image.at<Vec3b>(i, j)[0];
                m_vec_corner_regions[3].at<Vec3b>(i-nine_tenth_rows, j-nine_tenth_cols)[1] = m_src_image.at<Vec3b>(i, j)[1];
                m_vec_corner_regions[3].at<Vec3b>(i-nine_tenth_rows, j-nine_tenth_cols)[2] = m_src_image.at<Vec3b>(i, j)[2];
            }
        }
    }
}

void ShadingAlgorithm::ComputeAllAverageYValue()
{
    m_yc = double(ComputeAverageYValue(m_center_region));
    for(int i = 0; i < m_kCornerNumber; i++) 
    {
		if (m_yc != 0.0)
		{
			double shading_value = double(ComputeAverageYValue(m_vec_corner_regions[i])) / m_yc * 100;
			m_vec_ys.push_back(shading_value);
		}
		else
		{
			Logger(ERROR) << "Error: Denominator is zero.";
			throw DividedByZeroException();
		}
    }
}

int ShadingAlgorithm::ComputeAverageYValue(Mat img)
{
    int y_value = 0;
    for(int i = 0; i < img.rows; i++)
    {
        for(int j = 0; j < img.cols; j++)
        {
            int sum_of_rgb = img.at<Vec3b>(i, j)[0] + img.at<Vec3b>(i, j)[1] + img.at<Vec3b>(i, j)[2];
            y_value += sum_of_rgb/3;
        }
    }
    y_value /= img.rows*img.cols;
    return y_value;
}

bool ShadingAlgorithm::CheckCenterValue()
{
	// Return passed when y center value is between center low and center up
	bool result = m_yc > m_center_low && m_yc < m_center_up;
	return result;
}

bool ShadingAlgorithm::CheckPassLevelValue()
{
	// Return passed when y values in four corners are between pass level and pass level up
	bool result = CheckPassLevelValue(m_vec_ys[0]) 
		&& CheckPassLevelValue(m_vec_ys[1]) 
		&& CheckPassLevelValue(m_vec_ys[2]) 
		&& CheckPassLevelValue(m_vec_ys[3]);
	
	// Draw detected result on out image when the corner is shading
	for (int i = 0; i < m_kCornerNumber; i++)
	{
		if (!CheckPassLevelValue(m_vec_ys[i]))
		{
			Point pt1 = m_vec_y_coordinates[i + 1].first;
			Point pt2 = m_vec_y_coordinates[i + 1].second;
			rectangle(m_out_image, pt1, pt2, Scalar(0, 0, 255), 10, 8);
		}
	}
	return result;
}

bool ShadingAlgorithm::CheckPassLevelValue(double y_value)
{
	// Return passed when y value is between pass level and pass level up
	bool is_passed = y_value > m_pass_level && y_value < m_pass_level_up;
	return is_passed;
}

bool ShadingAlgorithm::CheckDiffValue()
{
	// Return passed when the difference of the max and the min in y values is smaller than diff
    vector<double>::iterator max, min;

    max = max_element(m_vec_ys.begin(), m_vec_ys.end());
    min = min_element(m_vec_ys.begin(), m_vec_ys.end());

	bool result = *max - *min < m_diff;
	return result;
}

void ShadingAlgorithm::RecordAndPrintResult()
{
	// Save result message
	m_result_vector.push_back("Shading,0,0,0," + ToPassOrFailString(m_is_passed));
	m_result_vector.push_back("Shading_TL,"
		+ to_string(m_vec_ys[0]) + ","
		+ to_string(m_pass_level_up) + ","
		+ to_string(m_pass_level) + ","
		+ ToPassOrFailString(CheckPassLevelValue(m_vec_ys[0])));
	m_result_vector.push_back("Shading_TR,"
		+ to_string(m_vec_ys[2]) + ","
		+ to_string(m_pass_level_up) + ","
		+ to_string(m_pass_level) + ","
		+ ToPassOrFailString(CheckPassLevelValue(m_vec_ys[2])));
	m_result_vector.push_back("Shading_BL,"
		+ to_string(m_vec_ys[1]) + ","
		+ to_string(m_pass_level_up) + ","
		+ to_string(m_pass_level) + ","
		+ ToPassOrFailString(CheckPassLevelValue(m_vec_ys[1])));
	m_result_vector.push_back("Shading_BR,"
		+ to_string(m_vec_ys[3]) + ","
		+ to_string(m_pass_level_up) + ","
		+ to_string(m_pass_level) + ","
		+ ToPassOrFailString(CheckPassLevelValue(m_vec_ys[3])));
	double diff_value = *max_element(m_vec_ys.begin(), m_vec_ys.end()) - *min_element(m_vec_ys.begin(), m_vec_ys.end());
	m_result_vector.push_back("Shading_Diff,"
		+ to_string(diff_value) + ","
		+ to_string(m_diff) + ","
		+ "0,"
		+ ToPassOrFailString(CheckDiffValue()));
	m_result_vector.push_back("Shading_Center,"
		+ to_string(m_yc) + ","
		+ to_string(m_center_up) + ","
		+ to_string(m_center_low) + ","
		+ ToPassOrFailString(CheckCenterValue()));

	// Print result message
	for (auto it = m_result_vector.begin(); it != m_result_vector.end(); it++)
	{
		Logger(INFO) << *it;
	}
}

string ShadingAlgorithm::GetTypeName()
{
	return "Shading";
}