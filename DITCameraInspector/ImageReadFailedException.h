#pragma once

#include <string>

#include "BaseCustomException.h"


namespace dit_camera_inspector
{
	class ImageReadFailedException : public BaseCustomException
	{
		public:
			ImageReadFailedException();
			ImageReadFailedException(string);

		protected:
			void SetErrorMessage();
	};
}


