#ifndef ALGORITHM_FACTORY
#define ALGORITHM_FACTORY

#include <map>
#include <string>

#include "Logger.h"
#include "IBaseAlgorithmRegistrar.h"
#include "AlgorithmNotRegisteredException.h"

using namespace std;

namespace dit_camera_inspector
{	
	// A class to store registered algorithms in a map and provide get algo function to user
    template <class ABSTRACT_ALGORITHM>
    class AlgorithmRegistry
    {
        public:
            // Use the singleton pattern, so that there is the only instance of the class
            static AlgorithmRegistry<ABSTRACT_ALGORITHM> &GetInstance();

            // Register algorithms with specified class name and key
            void RegisterAlgorithm(string, IBaseAlgorithmRegistrar<ABSTRACT_ALGORITHM>*);

            // Get algorithms in the factory database by specified key
			unique_ptr<ABSTRACT_ALGORITHM> GetAlgorithm(string, Spec*);

        private:
            // prevent from external access due to the singleton pattern
            AlgorithmRegistry() {}
			~AlgorithmRegistry();
            AlgorithmRegistry(const AlgorithmRegistry &);
            const AlgorithmRegistry &operator=(const AlgorithmRegistry &);
		public:
            // Store registered algorithms with key: an algorithm name, value: a proxy registrar to create algorithm 
            map<string, IBaseAlgorithmRegistrar<ABSTRACT_ALGORITHM>*> m_algorithm_registry;
    };
}
#include "AlgorithmRegistry.tpp"
#endif // ALGORITHM_FACTORY