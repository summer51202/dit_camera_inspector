#include "DividedByZeroException.h"

using namespace dit_camera_inspector;


DividedByZeroException::DividedByZeroException()
{
	SetErrorMessage();
}
DividedByZeroException::DividedByZeroException(string error_message) : BaseCustomException(error_message)
{

}
void DividedByZeroException::SetErrorMessage()
{
	m_error_message = "attempted to divide by zero";
}