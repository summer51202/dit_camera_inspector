#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <string>
#include <vector>
#include <direct.h>
#include <io.h>

#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;


enum LogLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    SAVE
};
class Logger
{
    public:
        Logger(LogLevel);
        ~Logger();
        template<class T>
        Logger& operator<<(const T&);
        static LogLevel& showLevel();
        static void SetVisibleLevel(LogLevel);
        string GetNowTime();
        string GetNowDate();
        string GetSerialNumber();
        void SaveCSVAndImage(vector<string>, Mat, string, string);


    private:
        string GetStringLogLevel(LogLevel);

        bool m_message_is_visible = false;
        LogLevel m_message_level = DEBUG;
        time_t m_now_time;
};

#include "Logger.tpp"
#endif // LOGGER_H