#pragma once
#include "BaseCustomException.h"

namespace dit_camera_inspector
{
	class AlgorithmNotRegisteredException : public BaseCustomException
	{
		public:
			AlgorithmNotRegisteredException();
			AlgorithmNotRegisteredException(string);

		protected:
			void SetErrorMessage();
	};

}

