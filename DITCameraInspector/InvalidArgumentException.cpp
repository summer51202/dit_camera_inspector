#include "InvalidArgumentException.h"

using namespace dit_camera_inspector;


InvalidArgumentException::InvalidArgumentException()
{
	SetErrorMessage();
}
InvalidArgumentException::InvalidArgumentException(string error_message) : BaseCustomException(error_message)
{

}
void InvalidArgumentException::SetErrorMessage()
{
	m_error_message = "invalid argument";
}