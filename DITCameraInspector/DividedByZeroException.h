#pragma once

#include <string>

#include "BaseCustomException.h"


namespace dit_camera_inspector
{
	class DividedByZeroException : public BaseCustomException
	{
		public:
			DividedByZeroException();
			DividedByZeroException(string);

		protected:
			void SetErrorMessage();
	};
}


