#ifndef SPEC_H
#define SPEC_H

#include <iostream>
#include <map>
#include <string>

using namespace std;

namespace dit_camera_inspector
{
    class Spec
    {
        public:
            void PrintContentMap();
            map<string, map<string, string>> m_content_map;
    };
}

#endif // SPEC_H