#include "BlemishAlgorithm.h"


unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, BlemishAlgorithm>> BlemishAlgorithm::m_is_registered = AlgorithmRegistrar<IImageInspectionAlgorithm, BlemishAlgorithm>::CreateRegistrar(BlemishAlgorithm::GetTypeName());

BlemishAlgorithm::BlemishAlgorithm(Spec* p_spec)
{
    m_gaussian_kernel_size = stoi(p_spec->m_content_map["Blemish"]["GaussianKernelSize"]);
    m_he_kernel_size = stoi(p_spec->m_content_map["Blemish"]["HEKernelSize"]);
    m_structural_kernel_size = stoi(p_spec->m_content_map["Blemish"]["StructuralKernelSize"]);
    m_dirty_area_ratio_thresh = stod(p_spec->m_content_map["Blemish"]["DirtyRatio"]);
	m_max_dirty_area_ratio = 0.0;
	m_type_name = "Blemish";
}
void BlemishAlgorithm::Run(Mat img)
{
    Logger(INFO) << "Blemish Inspecting...";

	// Init mat data
    m_src_image = img.clone();
	m_out_image = img.clone();

    // Convert to gray-scale image
    Mat gray_src;
    cvtColor(m_src_image, gray_src, COLOR_BGR2GRAY);

    // Gaussian blur
    GaussianBlur(gray_src, gray_src, Size(m_gaussian_kernel_size, m_gaussian_kernel_size), 1.0); 

    // Histogram equalization
    Ptr<CLAHE> ptrCLAHE = createCLAHE(70, Size(m_he_kernel_size, m_he_kernel_size));
    ptrCLAHE->apply(gray_src, gray_src);

    // Binarization
    Mat thresh_img;
    threshold(gray_src, thresh_img, 0, 255, THRESH_BINARY_INV | THRESH_OTSU);
    CV_Assert(thresh_img.empty() == false);

    // Erosion  
    Mat erosion_kernel = getStructuringElement(MORPH_RECT, Size(m_structural_kernel_size, m_structural_kernel_size));
    Mat morph_img;
    morphologyEx(thresh_img, morph_img, MORPH_ERODE, erosion_kernel);
    CV_Assert(morph_img.empty() == false);

    // Find the max contour
    vector<vector<Point>> contours;
    findContours(morph_img, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);
    double max_area = 0.0;
    int max_area_index = 0;
    for(int i = 0; i < contours.size(); i++)
    {
        double area = contourArea(contours[i]);
        if(area > max_area)
        {
            max_area = area;
            max_area_index = i;
        } 
    }

    drawContours(m_out_image, contours, max_area_index, Scalar(0, 0, 255), 10, 8);

	double src_image_area = m_src_image.rows*m_src_image.cols;
	if (src_image_area != 0.0)
	{
		m_max_dirty_area_ratio = max_area / (m_src_image.rows*m_src_image.cols);
	}
	else
	{
		Logger(ERROR) << "Error: Denominator is zero.";
		throw DividedByZeroException();
	}
	
	m_is_passed = m_max_dirty_area_ratio < m_dirty_area_ratio_thresh;

    Logger(DEBUG) << "Final result: " << m_is_passed;
}
void BlemishAlgorithm::ShowGrayImgHistogram(Mat img)
{
	int histSize = 256;
	float range[] = { 0, 256 }; //the upper boundary is exclusive
	const float* histRange[] = { range };
	bool uniform = true, accumulate = false;

	Mat hist;
	calcHist(&img, 1, 0, Mat(), hist, 1, &histSize, histRange, uniform, accumulate);
	int hist_w = 512, hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}

	namedWindow("hist img", WINDOW_NORMAL);
	imshow("hist img", histImage);
	waitKey(0);
}

void BlemishAlgorithm::RecordAndPrintResult()
{
	// Save result message
	m_result_vector.push_back("Blemish,0,0,0," + ToPassOrFailString(m_is_passed));
	m_result_vector.push_back("Blemish_DirtyRatio,"
		+ to_string(m_max_dirty_area_ratio) + ","
		+ to_string(m_dirty_area_ratio_thresh) + ",0,"
		+ ToPassOrFailString(m_is_passed));

	// Print result message
	for (auto it = m_result_vector.begin(); it != m_result_vector.end(); it++)
	{
		Logger(INFO) << *it;
	}
}

string BlemishAlgorithm::GetTypeName()
{
	return "Blemish";
}