#ifndef ALGORITHM_REGISTRAR
#define ALGORITHM_REGISTRAR

#include <string>
#include <memory>

#include "IBaseAlgorithmRegistrar.h"
#include "AlgorithmRegistry.h"
#include "Spec.h"

using namespace std;


namespace dit_camera_inspector
{
	// A algorithm registrar template to create algorithm instances and can be created in algorithm classes to register algorithms
    template <class ABSTRACT_ALGORITHM, class CONCRETE_ALGORITHM>
    class AlgorithmRegistrar : public IBaseAlgorithmRegistrar<ABSTRACT_ALGORITHM>
    {
        public:
            AlgorithmRegistrar(string);
			~AlgorithmRegistrar();
			static unique_ptr<AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>> CreateRegistrar(string);
			unique_ptr<ABSTRACT_ALGORITHM> CreateAlgorithm(Spec*);
    };
}
#include "AlgorithmRegistrar.tpp"
#endif // ALGORITHM_REGISTRAR