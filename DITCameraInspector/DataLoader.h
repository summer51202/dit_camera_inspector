#ifndef DATA_LOADER_H
#define DATA_LOADER_H

#include <iostream>
#include <fstream>

#include "opencv2/opencv.hpp"
#include "Logger.h"
#include "Spec.h"
#include "ImageReadFailedException.h"

using namespace std;
using namespace cv;

namespace dit_camera_inspector
{
    class DataLoader
    {
        public:
            DataLoader(string, string);
            ~DataLoader();
			void Load();
			Spec* GetSpec();
			Mat GetImage();
			void static ShowImage(Mat);

		private:
            void LoadSpec(string);
            bool IsTag(string);
            string TrimData(string);
            void LoadImage(string);
            
			string m_spec_path;
			string m_image_path;
            Spec *m_p_spec;
            Mat m_image;
    };

}
#endif // DATA_LOADER_H