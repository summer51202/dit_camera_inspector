#include "AlgorithmRegistry.h"

using namespace dit_camera_inspector;


template <class ABSTRACT_ALGORITHM>
AlgorithmRegistry<ABSTRACT_ALGORITHM>::~AlgorithmRegistry<ABSTRACT_ALGORITHM>()
{
	// TODO: release map memory manually
}

template <class ABSTRACT_ALGORITHM>
AlgorithmRegistry<ABSTRACT_ALGORITHM> &AlgorithmRegistry<ABSTRACT_ALGORITHM>::GetInstance()
{
    static AlgorithmRegistry<ABSTRACT_ALGORITHM> g_instance;
    return g_instance;
}

// Register algorithms with specified class name and key
template <class ABSTRACT_ALGORITHM>
void AlgorithmRegistry<ABSTRACT_ALGORITHM>::RegisterAlgorithm(string name, IBaseAlgorithmRegistrar<ABSTRACT_ALGORITHM> *p_registrar)
{
    m_algorithm_registry[name] = p_registrar;
}

// Get algorithms in the factory database by specified key
// The arguments of algorithm constructor are passed in this function as well
template <class ABSTRACT_ALGORITHM>
unique_ptr<ABSTRACT_ALGORITHM> AlgorithmRegistry<ABSTRACT_ALGORITHM>::GetAlgorithm(string name, Spec* p_spec)
{
    // return the found registered algorithm 
    if (m_algorithm_registry.find(name) != m_algorithm_registry.end())
    {
        return m_algorithm_registry[name]->CreateAlgorithm(p_spec);
    }

    // Unregistered algorithm and throw exception
    Logger(ERROR) << "No algorithm found for " << name;
	throw AlgorithmNotRegisteredException();
    return NULL;
}