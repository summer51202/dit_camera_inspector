#ifndef SHADING_ALGORITHM
#define SHADING_ALGORITHM

#include <string>
#include <memory>

#include "opencv2/opencv.hpp"
#include "IImageInspectionAlgorithm.h"
#include "Spec.h"
#include "Logger.h"
#include "DividedByZeroException.h"
#include "DataLoader.h"
#include "AlgorithmRegistrar.h"

using namespace std;
using namespace cv;
using namespace dit_camera_inspector;


namespace dit_camera_inspector
{
    class ShadingAlgorithm : public IImageInspectionAlgorithm
    {
        public:
            ShadingAlgorithm(Spec*);
            void Run(Mat);
			void RecordAndPrintResult();
        
        private:
            void GetCenterRegion();
            void GetCornerRegions();
            void ComputeAllAverageYValue();
            int ComputeAverageYValue(Mat);
            bool CheckCenterValue();
            bool CheckPassLevelValue();
			bool CheckPassLevelValue(double);
            bool CheckDiffValue();
			static string GetTypeName();

			static unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, ShadingAlgorithm>> m_is_registered;
            double m_pass_level_up;
            double m_pass_level;
            double m_diff;
            double m_center_up;
            double m_center_low;
            double m_yc;
            const int m_kCornerNumber = 4;
            Mat m_center_region;
            vector<Mat> m_vec_corner_regions;
            vector<double> m_vec_ys;
			vector<pair<Point, Point>> m_vec_y_coordinates;
    };  
}
#endif // SHADING_ALGORITHM