#ifndef I_BASE_ALGORITHM_REGISTRAR
#define I_BASE_ALGORITHM_REGISTRAR

#include <memory>

namespace dit_camera_inspector
{
    template <class ABSTRACT_ALGORITHM>
    class IBaseAlgorithmRegistrar
    {
        public:
            virtual unique_ptr<ABSTRACT_ALGORITHM> CreateAlgorithm(Spec*) = 0;
			virtual ~IBaseAlgorithmRegistrar() {}

        protected:
            // Prevent from instance construction
            IBaseAlgorithmRegistrar() {}
            
        private:
            // Prohibit the usage of the copy and assignment operators
            IBaseAlgorithmRegistrar(const IBaseAlgorithmRegistrar &);
			const IBaseAlgorithmRegistrar &operator=(const IBaseAlgorithmRegistrar &) {}
    };
}
#endif // I_BASE_ALGORITHM_REGISTRAR