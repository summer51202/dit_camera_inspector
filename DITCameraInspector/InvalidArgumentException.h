#pragma once
#include "BaseCustomException.h"

namespace dit_camera_inspector
{
	class InvalidArgumentException : public BaseCustomException
	{
	public:
		InvalidArgumentException();
		InvalidArgumentException(string);

	protected:
		void SetErrorMessage();
	};

}


