#include "Spec.h"

void dit_camera_inspector::Spec::PrintContentMap()
{
    for(auto it = m_content_map.rbegin(); it != m_content_map.rend(); it++)
    {
        cout << "{" << it->first << ": ";
        for(auto it2 = it->second.rbegin(); it2 != it->second.rend(); it2++)
        {
            cout << "{" << it2->first << ": " << it2->second << "}";
        }
        cout << "}" << endl;
    }
}