#include "Inspector.h"

using namespace dit_camera_inspector;


Inspector::Inspector(shared_ptr<Parser> p_args):m_p_args(p_args), m_p_loader(NULL)
{
}

Inspector::~Inspector()
{
	if (m_p_loader)
	{
		delete m_p_loader;
		m_p_loader = NULL;
	}
}

void Inspector::Run()
{
	LoadData();
	LoadAlogorithmAndExecute();
	SaveResult();
}

void Inspector::LoadData()
{
	if (!m_p_loader)	// Check for duplicated object constructions to prevent from memory leak when this function is called more than once
	{
		m_p_loader = new DataLoader(m_p_args->GetSpecFilePath(), m_p_args->GetImagePath());
		if (!m_p_loader)	// Check for object new failed
		{
			Logger(ERROR) << "Object new failed.";
		}
	}
	m_p_loader->Load();
}

void Inspector::LoadAlogorithmAndExecute()
{
	m_command_type = m_p_args->GetCommandList()[0];
	m_p_inspection_algorithm = AlgorithmRegistry<IImageInspectionAlgorithm>::GetInstance().GetAlgorithm(m_command_type, m_p_loader->GetSpec());
	m_p_inspection_algorithm->Run(m_p_loader->GetImage());
}

void Inspector::SaveResult()
{
	m_p_inspection_algorithm->RecordAndPrintResult();
	Logger(SAVE).SaveCSVAndImage(m_p_inspection_algorithm->GetResultVector(), m_p_inspection_algorithm->GetResultImage(), m_p_args->GetSpecFilePath(), m_p_args->GetImagePath());
}