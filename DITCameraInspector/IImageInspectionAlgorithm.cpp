#include "IImageInspectionAlgorithm.h"

using namespace dit_camera_inspector;


IImageInspectionAlgorithm::IImageInspectionAlgorithm() : m_is_passed(true)
{

}
IImageInspectionAlgorithm::~IImageInspectionAlgorithm()
{
	Logger(DEBUG) << "dtor IImageInspectionAlgorithm";
}
vector<string> IImageInspectionAlgorithm::GetResultVector()
{
	return m_result_vector;
}

Mat IImageInspectionAlgorithm::GetResultImage()
{
	if (m_is_passed)
		return m_src_image;
	else
		return m_out_image;
}

string IImageInspectionAlgorithm::ToPassOrFailString(bool result)
{
	string str_result;
	result ? str_result = "PASS" : str_result = "FAIL";
	return str_result;
}
