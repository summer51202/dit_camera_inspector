#include <iostream>

using namespace std;


template<class T>
Logger& Logger::operator<<(const T& message)
{
    if(m_message_level >= showLevel())
    {
        // Show log message
        cout << message;
        m_message_is_visible = true;
    }
    return *this;
}