#ifndef INSPECTOR
#define INSPECTOR

#include <string>

#include "Logger.h"
#include "Dataloader.h"
#include "Parser.h"
#include "IBaseAlgorithmRegistrar.h"
#include "AlgorithmRegistrar.h"
#include "AlgorithmRegistry.h"
#include "IImageInspectionAlgorithm.h"
#include "ShadingAlgorithm.h"
#include "BlemishAlgorithm.h"
#include "FlareAlgorithm.h"
#include "InvalidArgumentException.h"

using namespace std;

namespace dit_camera_inspector
{
    class Inspector
    {
        public:
            Inspector(shared_ptr<Parser>);
            ~Inspector();
            void Run();
        
		private:
			void LoadData();
			void LoadAlogorithmAndExecute();
			void SaveResult();

			shared_ptr<Parser> m_p_args;
            DataLoader *m_p_loader;
			string m_command_type;
			unique_ptr<IImageInspectionAlgorithm> m_p_inspection_algorithm;	
    };
}
#endif // INSPECTOR