#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <string>
#include <vector>

#include "Logger.h"

using namespace std;


namespace dit_camera_inspector
{
    class Parser
    {
        public:
            Parser(int, char**);
            bool ParseArgs();
            void ShowUsage();
			int GetArgc();
			char** GetArgv();
			string GetSpecFilePath();
			string GetImagePath();
			vector<string> GetCommandList();

		private:
			bool IsMoreThanMinArgc();
			bool ParseCommandAndPath();

            int m_argc = 0;
            char** m_argv;
            string m_spec_file_path;
            string m_image_path;
            vector<string> m_command_list;                           
    };
}

#endif // PARSER_H