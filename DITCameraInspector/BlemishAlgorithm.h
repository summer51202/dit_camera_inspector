#ifndef BLEMISH_ALGORITHM
#define BLEMISH_ALGORITHM

#include <string>
#include <memory>

#include "opencv2/opencv.hpp"
#include "Logger.h"
#include "IImageInspectionAlgorithm.h"
#include "Spec.h"
#include "DividedByZeroException.h"
#include "AlgorithmRegistrar.h"

using namespace std;
using namespace cv;
using namespace dit_camera_inspector;

namespace dit_camera_inspector
{
    class BlemishAlgorithm : public IImageInspectionAlgorithm
    {
        public:
            BlemishAlgorithm(Spec*);
            void Run(Mat);
			void RecordAndPrintResult();
           
        private:
            void ShowGrayImgHistogram(Mat);
			static string GetTypeName();

			static unique_ptr<AlgorithmRegistrar<IImageInspectionAlgorithm, BlemishAlgorithm>> m_is_registered;
			double m_dirty_area_ratio_thresh;
            int m_gaussian_kernel_size;
            int m_he_kernel_size;
            int m_structural_kernel_size;
			double m_max_dirty_area_ratio;
    };  

}
#endif // BLEMISH_ALGORITHM