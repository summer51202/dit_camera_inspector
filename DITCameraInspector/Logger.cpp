#include <iostream>
#include "Logger.h"


Logger::Logger(LogLevel level):m_message_level(level)
{
    // Show log level and current time
    operator << ("[" + GetStringLogLevel(m_message_level) + "]") << ("[" + GetNowTime() + "]");
}
Logger::~Logger()
{
    if(m_message_is_visible)
    {
        cout << endl;
    }
}

LogLevel& Logger::showLevel()
{
    static LogLevel level = INFO;
    return level;
}

void Logger::SetVisibleLevel(LogLevel level)
{
    showLevel() = level;
}

string Logger::GetStringLogLevel(LogLevel level)
{
    string str_level;
    switch(level)
    {
        case DEBUG: 
            str_level = "DEBUG";
            break;
        case INFO: 
            str_level = "INFO";
            break;
        case WARNING: 
            str_level = "WARNING";
            break;
        case ERROR: 
            str_level = "ERROR";
            break;
        case SAVE: 
            str_level = "SAVE";
            break;
    }
    return str_level;
}

string Logger::GetNowTime()
{
    time(&m_now_time);
    
    struct tm time_info;
    localtime_s(&time_info, &m_now_time);

    string s, m, h, D, M, Y;
    s = to_string(time_info.tm_sec);
    m = to_string(time_info.tm_min);
    h = to_string(time_info.tm_hour);
    D = to_string(time_info.tm_mday);
    M = to_string(time_info.tm_mon + 1);
    Y = to_string(time_info.tm_year + 1900);

    // Aligned output format  
    if (time_info.tm_sec < 10) s = "0" + s;
    if (time_info.tm_min < 10) m = "0" + m;
    if (time_info.tm_hour < 10) h = "0" + h;
    if (time_info.tm_mday < 10) D = "0" + D;
    if (time_info.tm_mon + 1 < 10) M = "0" + M;

    return (Y + M + D + " " + h + "h" + m + "m" + s + "s");
}

string Logger::GetNowDate()
{
    time(&m_now_time);
    
    struct tm time_info;
    localtime_s(&time_info, &m_now_time);

    string D, M, Y;
    D = to_string(time_info.tm_mday);
    M = to_string(time_info.tm_mon + 1);
    Y = to_string(time_info.tm_year + 1900);

    // Aligned output format  
    if (time_info.tm_mday < 10) D = "0" + D;
    if (time_info.tm_mon + 1 < 10) M = "0" + M;

    return (Y + M + D);
}
string Logger::GetSerialNumber()
{
    string number = "0.0.0.1";
    return number;
}

void Logger::SaveCSVAndImage(vector<string> log_message_vector, Mat log_image, string spec_file_path, string image_file_path)
{
	// Assemble file path and new a folder if the path is not exist 
    string folder_path = "D:/Edward/projects/CPlusPlus/DITCameraInspector/DITCameraInspector/ASUS_Camera_Reports_DLL/" + GetNowDate();
    if(_access(folder_path.c_str(), 0) == -1) 
		_mkdir(folder_path.c_str());

    folder_path += ("/" + GetSerialNumber());
    if(_access(folder_path.c_str(), 0) == -1) 
		_mkdir(folder_path.c_str());

    string csv_path = folder_path + "/Report-" + GetSerialNumber() + "-" + GetNowTime() + ".csv";

	// Save csv log message file
    ofstream file;
    file.open(csv_path.c_str());
	if (!file.is_open())
	{
		Logger(ERROR) << "Error: File opened failed.";
		file.exceptions(ofstream::failbit | ofstream::badbit);
	}

    file << "Report_Version: " << GetSerialNumber() << "-Project_Name: DITCameraInspector" << endl;
    file << "ITEM,SCORE,UPPER_BOUND,LOWER_BOUND,RESULT,SPEC_NAME,IMG,DATE" << endl;
	for (auto it = log_message_vector.begin(); it != log_message_vector.end(); it++)
	{
		file << *it << "," << spec_file_path << "," << image_file_path << "," << GetNowTime() << endl;
	}
    file.close();

	// Print the target csv path on console -> [SAVE] {csv_path}
    operator << (csv_path) << " is saved. ";

	// Parse log message to assmeble the target image file path
    vector<string> log_messages;
    stringstream ss(log_message_vector[0]);

    while (ss.good()) 
    {
        string substr;
        getline(ss, substr, ',');
        log_messages.push_back(substr);
    }

    string img_path = folder_path + "/Report-" + GetSerialNumber() + "-" + GetNowDate() + "-" + log_messages[4] + "-" + log_messages[0] + "-DEBUG.jpg";

	// Save image file
    imwrite(img_path.c_str(), log_image);

	// Print the target image path on console -> [SAVE] {img_path}
	operator << (img_path) << " is saved.";
}
