#include "AlgorithmRegistrar.h"

using namespace dit_camera_inspector;

template <class ABSTRACT_ALGORITHM, class CONCRETE_ALGORITHM>
AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>::AlgorithmRegistrar(string name)
{
    // Register algorithm by AlgorithmFactory
    AlgorithmRegistry<ABSTRACT_ALGORITHM>::GetInstance().RegisterAlgorithm(name, this);
}

template <class ABSTRACT_ALGORITHM, class CONCRETE_ALGORITHM>
AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>::~AlgorithmRegistrar()
{
	Logger(DEBUG) << "dtor AlgorithmRegistrar";
}

template <class ABSTRACT_ALGORITHM, class CONCRETE_ALGORITHM>
unique_ptr<AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>> AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>::CreateRegistrar(string command_type)
{
	return make_unique<AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>>(command_type);
}

template <class ABSTRACT_ALGORITHM, class CONCRETE_ALGORITHM>
unique_ptr<ABSTRACT_ALGORITHM> AlgorithmRegistrar<ABSTRACT_ALGORITHM, CONCRETE_ALGORITHM>::CreateAlgorithm(Spec* p_spec)
{
    return make_unique<CONCRETE_ALGORITHM>(p_spec);
}


