#include "DataLoader.h"

using namespace dit_camera_inspector;

DataLoader::DataLoader(string spec_path, string image_path): m_spec_path(spec_path), m_image_path(image_path), m_p_spec(NULL)
{
}
DataLoader::~DataLoader()
{
	if (m_p_spec)
	{
		delete m_p_spec;
		m_p_spec = NULL;
	}
}
void DataLoader::Load()
{
	LoadSpec(m_spec_path);
	LoadImage(m_image_path);
}
void DataLoader::LoadSpec(string spec_path)
{
    Logger(INFO) << "Loading spec file...";

    ifstream spec_in_file(spec_path.c_str());
	// TODO: too deep nested structure?
    if(spec_in_file.is_open())
    {
		if (!m_p_spec)	// Check for duplicated object constructions to prevent from memory leak when this function is called more than once
		{
			m_p_spec = new Spec();
			if (!m_p_spec)
			{
				Logger(ERROR) << "Error: Object new failed.";
			}
		}
    }
    else
    {
		Logger(ERROR) << "Error: File opened failed.";
		spec_in_file.exceptions(ifstream::failbit | ifstream::badbit);
    }

    // Recognize the tag in the spec file
    string line, tag, attr[2];
    while(getline(spec_in_file, line))
    {
        if(!line.empty())
        {
            if(IsTag(line))
            {
                tag.assign(line.begin()+1, line.end()-1);
                m_p_spec->m_content_map[tag]["Algorithmlevel"] = tag;
            }
            else
            {
                stringstream line_stream(line);
                string line_data;
                int i = 0;
                while(getline(line_stream, line_data, '\t')) 
                {
                    if(!line_data.empty())
                    {
                        attr[i] = TrimData(line_data);
                        i++;
                    }
                }
                m_p_spec->m_content_map[tag][attr[0]] = attr[1];
            }
        }
    }
}
bool DataLoader::IsTag(string line)
{
    // Check if the input string is tag-formatted
    // Ex: [string]
	bool result = *(line.begin()) == '[' && *(line.end() - 1) == ']';
	return result;
}
string DataLoader::TrimData(string str)
{
    // If str starts with "=", trim it
    // Ex: "= 200" --> "200"
	string result = *(str.begin()) == '=' ? str.assign(str.begin() + 2, str.end()) : str;
	return result;
}
void DataLoader::LoadImage(string imagePath)
{
    Logger(INFO) << "Loading images...";
    m_image = imread(imagePath);
    if(m_image.empty())
    {
        Logger(ERROR) << "Error: Empty Image or Invalid Image Path. " << imagePath;
		throw ImageReadFailedException();
    }
	Logger(INFO) << imagePath << " opened successful.";
}
Spec* DataLoader::GetSpec()
{
	return m_p_spec;
}
Mat DataLoader::GetImage()
{
	return m_image;
}
void DataLoader::ShowImage(Mat img)
{
    namedWindow("Test Image", WINDOW_NORMAL);
    imshow("Test Image", img);
    waitKey(0);
    destroyAllWindows();
}