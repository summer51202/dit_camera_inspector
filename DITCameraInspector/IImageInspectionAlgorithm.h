#ifndef I_IMAGE_INSPECTION_ALGORITHM
#define I_IMAGE_INSPECTION_ALGORITHM

#include <string>
#include <vector>

#include "opencv2/opencv.hpp"
#include "Logger.h"


using namespace std;
using namespace cv;

namespace dit_camera_inspector 
{
    class IImageInspectionAlgorithm
    {
        public:
            virtual void Run(Mat) = 0;
			virtual void RecordAndPrintResult() = 0;
			virtual ~IImageInspectionAlgorithm();
			vector<string> GetResultVector();
			Mat GetResultImage();
        
        protected:
			IImageInspectionAlgorithm();
			string ToPassOrFailString(bool);

			string m_type_name;
			Mat m_src_image;
			Mat m_out_image;
			vector<string> m_result_vector;
			bool m_is_passed;
    };
}
#endif // I_IMAGE_INSPECTION_ALGORITHM