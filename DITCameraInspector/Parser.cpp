#include "Parser.h"

using namespace dit_camera_inspector;

Parser::Parser(int argc, char** argv):m_argc(argc), m_argv(argv)
{
}
bool Parser::ParseArgs()
{
	if (IsMoreThanMinArgc() && ParseCommandAndPath())
	{
		for (auto it = m_command_list.begin(); it != m_command_list.end(); it++)
		{
			Logger(DEBUG) << "Command: " << *(it);
			return true;
		}
	}
	else
		return false;
}
bool Parser::IsMoreThanMinArgc()
{
	if (m_argc < 4)
	{
		Logger(ERROR) << "Error: Invalid argument.";
		ShowUsage();
		return false;
	}
	else
		return true;
}
bool Parser::ParseCommandAndPath()
{
    for(int i = 1; i < m_argc; i++)
    {
        string arg = m_argv[i];
        if(arg == "--help" || arg == "-h")
        {
            ShowUsage();
			Logger(ERROR) << "Error: Invalid argument.";
			return false;
        } 
        else if(arg == "-LS[Shading]" || arg == "-BL[Blemish]" || arg == "-FL[Flare]")
        {
			string tag = arg.assign(arg.begin() + 4, arg.end() - 1);
			// Ex: push "Shading" rather than "[Shading]"
			m_command_list.push_back(tag);
        }
        // Debug mode on
        else if(arg == "-DEBUG")
        {
            Logger::SetVisibleLevel(DEBUG);
        }
        // Spec file path
        else if(i == m_argc - 2)
        {
            m_spec_file_path = arg;
        }
        // Image path
        else
        {
            m_image_path = arg;
        }
    }
	return true;
}
int Parser::GetArgc()
{
	return m_argc;
}
char** Parser::GetArgv()
{
	return m_argv;
}
string Parser::GetSpecFilePath()
{
	return m_spec_file_path;
}
string Parser::GetImagePath()
{
	return m_image_path;
}
vector<string> Parser::GetCommandList()
{
	return m_command_list;
}
void Parser::ShowUsage(){
    Logger(INFO) << "Usage: DITCameraInspector.exe {inspection_type} {spec_file_path} {image_path}";
	Logger(INFO) << "inspection_type: -LS[Shading], -BL[Blemish], -FL[Flare]";
}
